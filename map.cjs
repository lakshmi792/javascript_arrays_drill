const items = [1, 2, 3, 4, 5, 5];


function map(item, cb) {
    
    if(item.length==0){
        return [];
    }
    let newArray = [];
    for(let index=0;index<item.length;index++){
        newArray.push(cb(item[index],index,item));
    }
    
    return newArray;
}

module.exports = map;