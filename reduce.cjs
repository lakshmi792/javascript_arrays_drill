function reduce(item, callback, startValue) {
    if (item.length == 0) {
        return [];
    }

    if (startValue === undefined) {
        startValue = item[0];
    }
    else {
        startValue = callback(startValue, item[0]);
    }

    for (let index = 1; index < item.length; index++) {
        startValue = callback(startValue, item[index], index, item);
    }


    return startValue;

}


module.exports = reduce;