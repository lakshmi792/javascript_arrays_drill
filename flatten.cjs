const items = [1, [2], [[3]], [[[4]]]];

function flatten(item, depth = 1) {
    if (item.length == 0) {
        return [];
    }

    let flatArray = [];
    for (let index = 0; index < item.length; index++) {
        if (Array.isArray(item[index]) && depth >= 1) {
            flatArray = flatArray.concat(flatten(item[index], depth - 1));
        }
        else {
            if (item[index] === undefined || item[index] === null) {

                continue;
            }
            else {
                flatArray.push(item[index]);
            }


        }
    }
    return flatArray;
}

module.exports = flatten;
